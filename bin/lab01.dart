import 'dart:io';
import 'dart:math';
void main(List<String> arguments) {
  String list = (stdin.readLineSync()!);

  print(Calculator(list).Evaluate);
  
}

List TokenizingaString(String input) {
  List<String> list = [];
  String temp = "";
  for (int i = 0; i < input.length; i++) {
    if (input[i] == " ") {
      if (temp.isEmpty) {
      } else {
        list.add(temp);
        temp = "";
      }
    } else {
      temp = temp + input[i];
    }
  }
  if (input[input.length - 1].contains("")) {
    list.add(temp);
  }
  return list;
}

List InfixToPostfix(List input2) {
  List<String> listoperators = [];
  List<String> listpostfix = [];
  for (int i = 0; i < input2.length; i++) {
    if (input2[i] == '(') {
      listoperators.add(input2[i]);
    } else if (input2[i] == ')') {
      while (listoperators.last != '(') {
        listpostfix.add(listoperators.last);
        listoperators.removeLast();
      }
      listoperators.removeLast();
    } else if (input2[i] == '+' || input2[i] == '-' || input2[i] == '*' || input2[i] == '/' || input2[i] == '^') {
      while (listoperators.isNotEmpty && listoperators.last != '(' && precedence(input2[i]) <= precedence(listoperators.last)) {
        listpostfix.add(listoperators.last);
        listoperators.removeLast();
      }
      listoperators.add(input2[i]);
    } else if (int.parse(input2[i]) is int) {
      listpostfix.add(input2[i]);
    }
  }
  while(listoperators.isNotEmpty){
    listpostfix.add(listoperators.last);
    listoperators.removeLast();
  }
  return listpostfix;
}

int precedence(var operators) {
  int tier = 0;
  if (operators == '+' || operators == '-') {
    tier = 1;
  } else if (operators == '*' || operators == '/') {
    tier = 2;
  } else if (operators == '^') {
    tier = 3;
  }
  return tier;
}
num EvaluatePostfix(List input3){
  List<num> values = [];
  num right = 0;
  num left = 0;
  for(int i=0; i<input3.length; i++){
    if(double.tryParse(input3[i]) is double){  
      values.add(double.parse(input3[i]) );
    }else{
      right = values.last;
      values.removeLast();
      left = values.last;
      values.removeLast();
      if(input3[i]=='+'){
        values.add(left+right);
      }else if(input3[i]=='-'){
         values.add(left-right);
      }else if(input3[i]=='*'){
         values.add(left*right);
      }else if(input3[i]=='/'){
         values.add(left/right);
      }else if(input3[i]=='^'){
         values.add(pow(left, right));
      }
     }
  }
  return values.first;
}

class Calculator{
      List Token = [];
      List Postfix = [];
      num Evaluate = 0;
    Calculator(String input){
      Token = TokenizingaString(input);
      Postfix = InfixToPostfix(Token);
      Evaluate = EvaluatePostfix(Postfix);
    }num ans(){
      return Evaluate;
}

}


