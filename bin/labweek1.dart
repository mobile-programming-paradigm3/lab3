
void main(List<String> arguments) {
  // declaring an object s1 of the class
  //1
  var s1 = square();

  // initializing object's attribute side with value 4
  s1.side = 4;

  // calling the method area of the class. Variable a is used to hold the value returned by the function
  int a = s1.area(s1.side);

  // printing the side of the square and it’s area using string interpolation
  print(" \n Side of the square is : ${s1.side} ");
  print(" \n Area of the square is : $a ");
  //2
  Student s2 = Student();
  s2.setName = " This is the tutorial for classes ! ";
  print(" Hey, ${s2.getName} ");

  //3
  Addition a1 = new Addition();
  int sum;

  sum = a1.sum(5, 4);
  print('sum = $sum');
//4
Student1 ss1 = Student1('123','John', 'Smith', '1 Jan 2000');
  Student1 ss2 = Student1.withoutBirthday('111', 'Jim', 'Null');


}
class square {   // defining a class
  var side;

  int area(int r) {
    int ar = r * r;
    return ar;
  }
}

class Student {
  String sName = ' null ';

  String get getName {
    return sName;
  }

  set setName(String name) {
    sName = name;
  }
}

class Addition {
  int a = 0;
  int b = 0;

  int sum(int a, int b) {
    this.a = a;
    this.b = b;
    return this.a + this.b;
  }
}

class Student1 {
  String stuID = 'NULL';
  String stuName = 'NULL';
  String stuSurName = 'NULL';
  bool giveBirthday = true;
  String stuBirthday = 'NULL';

  //Parameterized Constructor
  Student1(stuID, stuName, stuSurName, stuBirthday);

  //Named Constructor
  Student1.withoutBirthday(stuID, stuName, stuSurName) : giveBirthday = false;
}



